package cfg;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public abstract class INIManipuler {

    public static HashMap read(File file) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(file);
        BufferedReader r = new BufferedReader(fr);

        HashMap m = new HashMap();
        String linha;
        String title = "basic";
        HashMap i = new HashMap();

        while ((linha = r.readLine()) != null) {
            if ((linha.indexOf("[") != -1) && (linha.indexOf("]") != -1)) {
                title = linha.substring(linha.indexOf("[") + 1, linha.indexOf("]"));
            } else if (linha.indexOf("=") > 0) {
                if (!m.containsKey(title)) {
                    i = new HashMap();
                    m.put(title, i);
                }
                String[] celulas = linha.split("=");
                i.put(celulas[0], celulas[1]);
            }
        }
        return m;
    }

    public static void save(File file, HashMap info) throws IOException {
        FileWriter fw = new FileWriter(file);
        BufferedWriter w = new BufferedWriter(fw);

        Iterator titles = info.keySet().iterator();

        while (titles.hasNext()) {
            String title = (String) titles.next();
            w.write("[" + title + "]");
            w.newLine();
            HashMap paramBook = (HashMap) info.get(title);
            Iterator i_parametros = paramBook.keySet().iterator();
            while (i_parametros.hasNext()) {
                String parametro = (String) i_parametros.next();
//                System.out.println(parametro);
                Object valor = paramBook.get(parametro);
//                System.out.println(valor);
                w.write(parametro + "=" + valor);
                w.newLine();
            }
        }
        w.flush();
        w.close();
    }
}
