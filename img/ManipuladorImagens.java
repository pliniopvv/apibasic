/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package img;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;

/**
 *
 * @author Plinio
 */
public class ManipuladorImagens {

    /**
     * @param args the command line arguments
     */
//    public static void main(String[] args) throws FileNotFoundException, IOException {
//
////        BufferedImage a = ManipuladorImagens.load("D:/ARQUIVOS/Arquivos/imagens/maquinaFotos/FER_0737_PB_JoaoPessoa.JPG");
////        BufferedImage b = ManipuladorImagens.Reduzir(a, 800, 600);
////        ManipuladorImagens.save("PNG", "C:/Users/Plinio/Desktop/teste/ferias.png", a);
//
//
////        BufferedImage c = ManipuladorImagens.Reduzir(a, 364, 273);
////        BufferedImage d = ManipuladorImagens.Reduzir(a, 400, 300);
////        BufferedImage e = ManipuladorImagens.Reduzir(a, 400, 400);
////        ManipuladorImagens.save("PNG", "C:/Users/Plinio/Desktop/teste/thumbmail_ferias.png", b);
////        ManipuladorImagens.save("PNG", "C:/Users/Plinio/Desktop/teste/thumbmail_ferias_4profile.png", c);
////        ManipuladorImagens.save("PNG", "C:/Users/Plinio/Desktop/teste/mini_thumb.png", d);
////        ManipuladorImagens.save("PNG", "C:/Users/Plinio/Desktop/teste/distort_thumb.png", e);
//
//        BufferedImage image = ManipuladorImagens.load("D:/ARQUIVOS/@PERSONALIZACOES/PAGINAS/PAGINAS COM AJAX/Argumentum/"
//                + "web/Data/fotosPerfis/pliniovictor/FER_0754_PB_JoaoPessoa.JPG");
//
//        double fator = image.getWidth() / 200.0;
//
//        System.out.println("Fator de redução: " + fator);
//
//        int w = (int) (image.getWidth() / fator);
//        int h = (int) (image.getHeight() / fator);
//
//        ManipuladorImagens.save("JPG",
//                "D:/ARQUIVOS/@PERSONALIZACOES/PAGINAS/PAGINAS COM AJAX/Argumentum/web/Data/fotosPerfis/pliniovictor/fotoPerfil.JPG",
//                ManipuladorImagens.Reduzir(image, w, h));
//
////        try {
////            if (args[0] == "?") {
////                System.out.println("Digite os parâmetros para executar o aplicativo: \n"
////                        + "[Largura] [Comprimento] [Caminho da foto] [Caminho para salvar a foto].\n"
////                        + "                         App desenvolvido por GanGss (Beta)");
////            } else {    
////                System.out.println("Largura da foto: " +args[0]);
////                int w = Integer.parseInt(args[0]);
////                System.out.println("Comprimento da foto: " +args[1]);
////                int h = Integer.parseInt(args[1]);
////                String c = args[2];
////                System.out.println("Caminho de origem da foto: "+args[2]);
////                String d = args[3];
////                System.out.println("Caminho de destino da foto: "+args[3]);
////                ManipuladorImagens.save("PNG", d, ManipuladorImagens.Reduzir(ManipuladorImagens.load(c), w, h));
////            }
////        } catch (ArrayIndexOutOfBoundsException e) {
////            System.out.println("Digite os parâmetros para executar o aplicativo: \n"
////                    + "[Largura] [Comprimento] [Caminho da foto].\n"
////                    + "                         App desenvolvido por GanGss (Beta)");
////        } catch (Exception e) {
////            System.out.println("Animal, tu não sabe executar uma bosta de um app? contate o desenvolvedor!");
////        }
//
//
//    }

    /**
     *
     * @param image Entre com a imagem a ser reduzida.
     * @param width Largura da imagem.
     * @param heigth Comprimento da imagem.
     */
    public static BufferedImage load(String pathImage) throws FileNotFoundException, IOException {
        File f = new File(pathImage);
        FileImageInputStream fiis = new FileImageInputStream(f);
        BufferedImage bi = ImageIO.read(fiis);
        return bi;
    }

    public static boolean save(String formatImagem, String pathImagem, BufferedImage imagem) throws FileNotFoundException, IOException {
        File f = new File(pathImagem);
        return ImageIO.write(imagem, formatImagem, f);
    }

    public static BufferedImage Reduzir(BufferedImage image, int width, int heigth) {
        Image a = image.getScaledInstance(width, heigth, 1);
        BufferedImage b = new BufferedImage(width, heigth, BufferedImage.TYPE_INT_RGB);
        b.getGraphics().drawImage(a, 0, 0, null);
        b.flush();
        a.flush();
        return b;
    }

    public static BufferedImage getBufferedImage(Image imagem) {
        BufferedImage bi = new BufferedImage(imagem.getWidth(null), imagem.getHeight(null), BufferedImage.TYPE_INT_RGB);
        bi.getGraphics().drawImage(imagem, 0, 0, null);
        return bi;
    }

    public static BufferedImage CortarImagem(BufferedImage image, int width, int heigth, int x, int y) {
        BufferedImage bi = image.getSubimage(x, y, width, heigth);
        return bi;
    }
}
