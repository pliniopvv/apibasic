package win;

import java.io.*;

public abstract class HD {

    public static int[] openFile(String path) throws FileNotFoundException, IOException {
        File file = new File(path);
        return openFile(file);
    }

    public static int[] openFile(File file) throws FileNotFoundException, IOException {
        FileInputStream Arquivo = new FileInputStream(file);
        int[] ArquivoStream = new int[Arquivo.available()];
        int transferStream = 0;
        for (int i = 0; i < ArquivoStream.length; i++) {
            transferStream = Arquivo.read();
            ArquivoStream[i] = transferStream;
        }
        Arquivo.close();
        return ArquivoStream;
    }

    public static Object openFileObject(String path) throws FileNotFoundException, IOException, ClassNotFoundException {
        File file = new File(path);
        return openFileObject(path);
    }

    public static Object openFileObject(File Arquivo) throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream OS = new FileInputStream(Arquivo);
        ObjectInputStream Leitor = new ObjectInputStream(OS);
        return Leitor.readObject();
    }

    public static void saveFile(String path, int[] Conteudo) throws FileNotFoundException, IOException {
        File file = new File(path);
        saveFile(file, Conteudo);
    }

    public static void saveFile(File file, int[] Conteudo) throws FileNotFoundException, IOException {
        FileOutputStream Conversor = new FileOutputStream(file);
        for (int cada : Conteudo) {
            Conversor.write(cada);
        }
        Conversor.flush();
        Conversor.close();
    }

    public static void saveFileObject(String path, Object Objeto) throws FileNotFoundException, IOException {
        File file = new File(path);
        saveFileObject(file, Objeto);
    }

    public static void saveFileObject(File file, Object Objeto) throws FileNotFoundException, IOException {
        FileOutputStream Conversor = new FileOutputStream(file);
        ObjectOutputStream Escritor = new ObjectOutputStream(Conversor);
        Escritor.writeObject(Objeto);
    }

    public static String getPath(Class ob) {
        return ob.getResource("").getFile();
    }

    public static void move(String oldPath, String newPath) throws FileNotFoundException, IOException {
        File oldFile = new File(oldPath);
        File newFile = new File(newPath);
        move(oldFile, newFile);
    }

    public static void move(File oldFile, File newFile) throws FileNotFoundException, IOException {
        newFile.getParentFile().mkdirs();
        saveFile(newFile, openFile(oldFile));
        oldFile.delete();
    }

    public static void copy(File oldFile, File newFile) throws IOException {
        saveFile(newFile, openFile(oldFile));
    }
}
