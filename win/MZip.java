/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package win;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Plinio
 */
public class MZip {

    private File file;
    private ZipFile zipfile;
    private String dirInZip = "";

    public MZip() {
    }

    public MZip(File file) throws ZipException, IOException {
        load(file);
    }

    public void load(File file) throws ZipException, IOException {
        this.file = file;
        zipfile = new ZipFile(file);
    }

    public void close() throws IOException {
        zipfile.close();
    }

    public void join(String NomeDaEntrada) {
        dirInZip = NomeDaEntrada + "/";
    }

    public String dir() {
        return dirInZip;
    }

    public File getFile() {
        return file;
    }

    @Deprecated
    public Object open(String key) {
        return zipfile.getEntry(dirInZip + key);
    }

    public String readText(String pathInZip) throws IOException {
        ZipEntry ze = zipfile.getEntry(pathInZip);
        InputStreamReader isr = new InputStreamReader(zipfile.getInputStream(ze));
        BufferedReader br = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String read = br.readLine();

        while (read != null) {
            sb.append(read);
            read = br.readLine();
        }
        return sb.toString();
    }

    public Object readObject(String pathInZip) throws IOException, ClassNotFoundException {
        ZipEntry ze = zipfile.getEntry(pathInZip);
        ObjectInputStream ois = new ObjectInputStream(zipfile.getInputStream(ze));
        return ois.readObject();
    }

    public void saveFile(String path, String pathInZip) throws IOException, ClassNotFoundException {
        saveFile(new File(path), pathInZip);
    }

    public void saveFile(File file, String pathInZip) throws IOException, ClassNotFoundException {
        int[] fileInt = HD.openFile(file);
        save(fileInt, pathInZip);
    }

    public void saveObject(Object objeto, String pathInZip) throws IOException {
        PrintStream ps = new PrintStream(file);
        ZipOutputStream zos = new ZipOutputStream(ps);
        ObjectOutputStream oos = new ObjectOutputStream(zos);
        ZipEntry ze = zipfile.getEntry(pathInZip);
        zos.putNextEntry(ze);
        oos.writeObject(objeto);
        oos.close();
        zos.close();
        ps.close();
    }

    public void save(int[] dados, String pathInZip) throws FileNotFoundException, IOException {
        PrintStream ps = new PrintStream(this.file);
        ZipOutputStream zos = new ZipOutputStream(ps);
        BufferedOutputStream bos = new BufferedOutputStream(zos);
        ZipEntry ze = new ZipEntry(pathInZip);
        zos.putNextEntry(ze);

        for (int i : dados) {
            bos.write(i);
        }

        bos.flush();
        bos.close();
        zos.close();
        ps.close();
    }

    public List<String> filesList() {
        ArrayList<String> lista = new ArrayList<>();

        Enumeration<ZipEntry> e = (Enumeration<ZipEntry>) zipfile.entries();
        ZipEntry z;
        while (e.hasMoreElements()) {
            z = e.nextElement();
            lista.add(z.getName());
        }
        return lista;
    }

    public HashMap<String, byte[]> getFiles() throws FileNotFoundException, IOException {

        HashMap<String, byte[]> arquivos = new HashMap<>();

        Enumeration<ZipEntry> e = (Enumeration<ZipEntry>) zipfile.entries();
        ZipInputStream zis = new ZipInputStream(new FileInputStream(getFile()));
        BufferedInputStream bis = new BufferedInputStream(zis);

        ZipEntry z;
        while ((z = zis.getNextEntry()) != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int b;
            while ((b = zis.read()) != -1) {
                baos.write(b);
            }
//            if (!z.isDirectory()) {
//            byte[] b = new byte[zis.available()];
//            zis.read(b);
            arquivos.put(z.getName(), baos.toByteArray());
//            }
        }

        return arquivos;
    }

    public void compact(HashMap<String, byte[]> arquivos) throws IOException {
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(getFile()));

        for (String k : arquivos.keySet()) {
            zos.putNextEntry(new ZipEntry(k));
            zos.write(arquivos.get(k));
            zos.flush();
        }
//        zos.flush();
        zos.close();
    }

//    public static void main(String[] args) {
//        File savedFile = new File("teste/diario.qsex");
//        MZip zip = new MZip(savedFile);
//        
//        
//        
//        File savedFile = new File("teste/diario.qse");
//        File addFile = new File("teste/A.txt");
//        MZip zip = new MZip(savedFile);
//        zip.saveFile(addFile, "A");
//        zip.close();
//    }
}
