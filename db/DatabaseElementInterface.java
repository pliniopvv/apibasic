package db;

public interface DatabaseElementInterface {

    public String SQLUpdate();

    public String SQLExclude();

    public String SQLQuery();

    public String SQLInsert();

    public String getTabela();
}
