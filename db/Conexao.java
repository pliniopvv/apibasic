/* 
 * Created on 09/08/2004 
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * @author BBAmbrozio
 *
 * Classe responsável pela conexão com o banco de dados Firebird 1.5
 *
 * Database: horas User: sysdba Password: masterkey
 *
 */
public class Conexao {

    public Connection con = null;
    public Statement stm = null;

    public Conexao() {

        try {

            Class.forName("org.firebirdsql.jdbc.FBDriver");
            con =
                    DriverManager.getConnection(
                    //                    "jdbc:firebirdsql:25.107.163.158/3050:\\compartilhada\\@SYS\\DATABASE\\TESTE01.FDB",
                    "jdbc:firebirdsql:25.107.163.158/3050:I:\\@QSE CONSULTORIA\\COMPARTILHADA\\@SYS\\DATABASE\\TESTE01.FDB",
                    //
                    //                                        "jdbc:firebirdsql:localhost/3050:C:\\TESTE01.FDB",
                    //                    "jdbc:firebirdsql:localhost/3050:C:\\TESTE02.FDB",
                    "sysdba",
                    "masterkey");
            stm = con.createStatement();

        } catch (Exception e) {
            System.out.println("Não foi possível conectar ao banco: " + e.getMessage());
        }

    }
}
